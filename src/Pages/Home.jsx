import { useEffect, useState } from 'react'
import Products from "../components/Products/Products"

export default function Home({products, setProducts, favouritesList, setFavouritesList, cart, addToCart}){
  
    return (
      <Products cart={cart} addToCart={addToCart} products={products} favouritesList={favouritesList} setFavouritesList={setFavouritesList} />
    )
  }