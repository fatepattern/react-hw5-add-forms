import { useState } from "react";
import ProductsCard from "../components/Products/ProductsCard";
import ModalImage from "../components/Modal/ModalImage";
import { useDispatch, useSelector } from "react-redux";
import { updateCart } from "../redux/cart.slice";
import { closeDeleteModalWindow, openDeleteModalWindow } from "../redux/deleteModalWindow.slice";
import { openSuccessModalWindow, closeSuccessModalWindow } from "../redux/successModalWindow.slice"
import * as Yup from 'yup';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import Button from "../components/Button/Button";
import ModalSuccess from "../components/Modal/ModalSuccess"
import "./Cart.scss"

const validationSchema = Yup.object().shape({
    firstName: Yup.string().required("First name is required"),
    lastName: Yup.string().required("Last name is required"),
    age: Yup.number().required("Age is required").positive("Age must be a positive number"),
    address: Yup.string().required('Address is required'),
    phone: Yup.string().required('Phone number is required')
})

export default function Cart({addToCart, products, favouritesList, setFavouritesList}){

    const dispatch = useDispatch();
    const cart = useSelector(state => state.cart.cart);

    let deleteButtons = document.querySelectorAll(".add-to-cart-button");

    const isOpened = useSelector(state => state.deleteModal.isOpened);
    const productToDelete = useSelector(state => state.deleteModal.product);

    const isSuccessModalOpened = useSelector(state => state.successModal.isOpened);
    

    deleteButtons.forEach(button => {
        button.style.display = "none";
    })

    const handleDeleteButtonClick = (product) => {
        dispatch(openDeleteModalWindow(product))
    }

    const handleConfirmDelete = () => {
        const newCart = cart.filter(item => item.id !== productToDelete.id);
        dispatch(updateCart(newCart))
        localStorage.setItem('cartItems', JSON.stringify(newCart));
        dispatch(closeDeleteModalWindow())
    }

    console.log(cart);

    const handleSubmit = (values) => {
        dispatch(openSuccessModalWindow())
        console.log('Checkout:', values);
        dispatch(updateCart([]));
        localStorage.removeItem('cartItems');
      };

    return (

        
        <div className="cart">

            <div className="cart-form">

                <Formik initialValues={{
                    firstName: '',
                    lastName: '',
                    age: '',
                    address: '',
                    phone: ''
                }
                }
                validationSchema={validationSchema}
                onSubmit={handleSubmit}>

                    <Form>
                        <div className="form-control">
                            <label htmlFor="firstName">First Name</label>
                            <Field type="text" id="firstName" name="firstName" />
                            <ErrorMessage name="firstName" component="div" className="error-message" />
                        </div>
                        <div className="form-control">
                            <label htmlFor="lastName">Last Name</label>
                            <Field type="text" id="lastName" name="lastName" />
                            <ErrorMessage name="lastName" component="div" className="error-message" />
                        </div>
                        <div className="form-control">
                            <label htmlFor="age">Age</label>
                            <Field type="number" id="age" name="age" />
                            <ErrorMessage name="age" component="div" className="error-message" />
                        </div>
                        <div className="form-control">
                            <label htmlFor="address">Address</label>
                            <Field type="text" id="address" name="address" />
                            <ErrorMessage name="address" component="div" className="error-message" />
                        </div>
                        <div className="form-control">
                            <label htmlFor="phone">Phone</label>
                            <Field type="text" id="phone" name="phone" />
                            <ErrorMessage name="phone" component="div" className="error-message" />
                        </div>
                        <Button type="submit">Checkout</Button>
                    </Form>

            </Formik>
            
            </div>

            <div className="cart-list">

                {isOpened && (
                    <ModalImage
                    onClose={() => dispatch(closeDeleteModalWindow())}
                    firstText="Are you sure you want to remove this item from your cart?"
                    secondaryText="Remove item"
                    buttonOneTitle="Cancel"
                    buttonTwoTitle="Remove"
                    secondaryClick={handleConfirmDelete}
                    />
                )}

                {isSuccessModalOpened && (
                    <ModalSuccess
                    onClose={() => dispatch(closeSuccessModalWindow())}
                    firstText="Successful checkout"
                    buttonOneTitle="OK"
                    />
                )}

                

                {cart.map(item => (

                    <ProductsCard 
                        key={item.id} 
                        product={item}
                        products={products}
                        cart={cart} 
                        addToCart={(product) => dispatch(updateCart(product))}
                        favouritesList={favouritesList} 
                        setFavouritesList={setFavouritesList}
                    >

                        <div className="cart-list__delete-btn" onClick={() => handleDeleteButtonClick(item)}>
                            <img src="../../public/118584_x_icon.svg" alt="x-icon" />
                        </div>

                    </ProductsCard>
                ))}

            </div>

            
        </div>
    )
}