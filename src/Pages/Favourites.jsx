import { useDispatch, useSelector } from "react-redux";
import ProductsCard from "../components/Products/ProductsCard";

export default function Favourites({addToCart, cart, setFavouritesList}){

    const favourites = useSelector(state => state.favourites.favourites);

    console.log(favourites);

    return(
        <div className="favourites-list">
            {favourites.map(favourite => (
                <ProductsCard 
                key={favourite.id} 
                product={favourite}
                favouritesList={favourites} 
                setFavouritesList={setFavouritesList}
                addToCart={addToCart}
                cart={cart}/>
            ))}
        </div>
    )
}