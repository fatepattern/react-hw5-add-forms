import ProductsCard from "./ProductsCard";
import PropTypes from 'prop-types';
import React from 'react';

const Products = ({ products, cart, addToCart, favouritesList, setFavouritesList}) => {

    if (!products) {
        return null;
    }

    return(
        
        <div className="products-list">

            {products.map(product => (
                <ProductsCard 
                key={product.id} 
                product={product}
                products={products}
                cart={cart} 
                addToCart={addToCart}
                favouritesList={favouritesList} 
                setFavouritesList={setFavouritesList}
               />
            ))}

        </div>
    )
}

Products.propTypes = {
    products: PropTypes.array.isRequired,
};
  

export default Products;