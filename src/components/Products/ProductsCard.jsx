import {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import Button from '../Button/Button';
import FavouriteButton from '../Button/FavouriteButton';
import "./ProductsCard.scss"
import ModalImage from '../Modal/ModalImage';
import { openModalWindow, closeModalWindow } from "../../redux/modalWIndow.slice";
import { useDispatch, useSelector } from "react-redux";
 

const ProductsCard = ({product, cart, addToCart, products, favouritesList, setFavouritesList, children}) => {

    const [isFavourite, setFavourite] = useState(product.isFavourite);

    const isOpened = useSelector(state => state.modalWindow.isOpened);
    const dispatch = useDispatch();

    const handleOpenModal = () => {
        dispatch(openModalWindow(product));
    }


    useEffect(() => {
        const storedCartItems = localStorage.getItem('cartItems');
        if (storedCartItems) {
            addToCart(JSON.parse(storedCartItems));
        }

        const storedFavourites = JSON.parse(localStorage.getItem("favourites")) || [];

        setFavouritesList(storedFavourites);
        
        storedFavourites.forEach(favourite => {
            if(favourite.id === product.id){
                setFavourite(product)
            }
        });


    }, []);

    const cartItem = useSelector(state => state.modalWindow.product);

    const handleAddToCart = () => {
        const newCart = [...cart, cartItem];
        addToCart(newCart);
        dispatch(closeModalWindow())
        localStorage.setItem('cartItems', JSON.stringify(newCart));
    };

    const handleAddToFavourites = () => {

        let newFavouritesList = [...favouritesList];

        setFavourite(!isFavourite);


        if (isFavourite) {
            newFavouritesList = newFavouritesList.filter(item => item.id !== product.id);

        } else {
            newFavouritesList.push(product);
        }

        setFavouritesList(newFavouritesList);

        localStorage.setItem('favourites', JSON.stringify(newFavouritesList));

        if (newFavouritesList.length === 0 || favouritesList === null){
            localStorage.removeItem('favourites');
        }

    }
  

    return(
        <>

        {isOpened && (
            <ModalImage
              firstText = "Add Product!"
              secondaryText = "By clicking the “Yes, add“ button, product will be added."
              buttonOneTitle = "No, Cancel"
              buttonTwoTitle = "Yes, add"
              onClose={() => dispatch(closeModalWindow()) }
              img = {cartItem.image}
              secondaryClick = {handleAddToCart}
            />
          )}

        <div className='products-card'>
            <img className="products-card__image" src={product.image} alt={product.name} />
            
            <div className='product-card__info'>
                <p className='products-card__info--name'>{product.name}</p>
                <p className='products-card__info--price'>{product.price}$</p>

                <div className="products-card__buttons">
                    <Button className ="add-to-cart-button" onClick={handleOpenModal}>Add to cart</Button>
                    {children}
                    <FavouriteButton isFavourite={isFavourite} onClick={handleAddToFavourites}/>
                </div>
            </div>
        </div>
        </>
    )
}

ProductsCard.propTypes = {
    product: PropTypes.object.isRequired,
};
  

export default ProductsCard;