import React from 'react';
import {Modal, ModalBody, ModalClose, ModalFooter, ModalHeader, ModalWrapper} from "./Modal"
import "./ModalSuccess.scss"


const ModalSuccess = ({ content, onClose, firstText, buttonOneTitle}) => {
    return (
        <ModalWrapper onClickOutside={onClose}>
            <Modal>
                <ModalHeader>
                    <ModalClose onClick={onClose}/>
                </ModalHeader>
                <ModalBody>
                    <img className="modal-img" src="../../../public/check.png" alt="success" />
                    <p>{content}</p>
                </ModalBody>
                <ModalFooter firstClick={onClose} firstText={firstText} buttonOneTitle={buttonOneTitle}>

                </ModalFooter>
            </Modal>
        </ModalWrapper>
    )
}

export default ModalSuccess;