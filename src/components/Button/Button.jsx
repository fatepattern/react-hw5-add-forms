import React from 'react';
import "./Button.scss";

export default function Button({type, className, onClick, children}){
    return (
        <button type={type} className={className} onClick={onClick}>
            {children}
        </button>
    )
}
