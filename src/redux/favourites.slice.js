import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    favourites: []
}

const favouritesSlice = createSlice({
    name: "favourites",
    initialState,
    reducers: {
        updateFavourites: (state, action) => {
            state.favourites = action.payload;
        }
    }
})

export const {updateFavourites} = favouritesSlice.actions;

export default favouritesSlice.reducer;