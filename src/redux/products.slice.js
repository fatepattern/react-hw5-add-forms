import {createSlice, createAsyncThunk } from '@reduxjs/toolkit';

const initialState = {
    products: [],
    status: null
}

export const fetchProducts = createAsyncThunk('products/fetchProducts', async () => {
    try {
        const response = await fetch('../public/products.json');
        if (!response.ok) {
            throw new Error('Failed to fetch products');
        }
        const data = await response.json();
        return data;
    } catch (error) {
        throw error;
    }
})

const productsSlice = createSlice({
    name: 'products',
    initialState,
    reducers: {
    },
    extraReducers: (builder) => {
        builder
        .addCase(fetchProducts.pending, (state) => {
            state.status = 'pending'
        })
        .addCase(fetchProducts.rejected, (state) => {
            state.status = 'error'
        })
        .addCase(fetchProducts.fulfilled, (state, action) => {
            state.status = 'done',
            state.products = action.payload
        })
    }
});

export default productsSlice.reducer;
