import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isOpened: false
}

const successModalWindowSlice = createSlice({
    name: "sucess-modal",
    initialState,
    reducers: {
        openSuccessModalWindow: (state, action) => {
            state.isOpened = true;
        },
        closeSuccessModalWindow: (state, action) => {
            state.isOpened = false;
        }
    }
})

export const {openSuccessModalWindow, closeSuccessModalWindow} = successModalWindowSlice.actions;

export default successModalWindowSlice.reducer;